export const environment = {
  // A boolean when to set
  production: false,
  // Your spotify client id you have generate
  spotifyClientID: '',
  clientUrl: 'http://localhost:4200',
  spotifyApiUrl: 'https://api.spotify.com/v1'
};
