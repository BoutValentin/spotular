import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarPlaylistLinkComponent } from './navbar-playlist-link.component';

describe('NavbarPlaylistLinkComponent', () => {
  let component: NavbarPlaylistLinkComponent;
  let fixture: ComponentFixture<NavbarPlaylistLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarPlaylistLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarPlaylistLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
