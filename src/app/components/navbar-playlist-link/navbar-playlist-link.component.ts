import { Component, Input, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { SimplifiedPlaylistObject } from 'src/app/RessourcesObject/SimplifiedPlaylist.object';

@Component({
  selector: 'app-navbar-playlist-link',
  templateUrl: './navbar-playlist-link.component.html',
  styleUrls: ['./navbar-playlist-link.component.css']
})
export class NavbarPlaylistLinkComponent implements OnInit {
  @Input() simplifiedPlaylistObject:SimplifiedPlaylistObject
  isActive: boolean;
  constructor(private router: Router) {
    this.router.events.subscribe(url => {
      if (url instanceof NavigationStart) {
        this.isActive = `/playlist/${this.simplifiedPlaylistObject.id}` === url.url;
      }
    })
   }

  ngOnInit(): void {
  }

  handleClick(event) {
    event.preventDefault();
    this.router.navigateByUrl(`/playlist/${this.simplifiedPlaylistObject.id}`)
  }
}
