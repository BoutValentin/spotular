import { Component, OnInit } from '@angular/core';
import { SimplifiedPlaylistObject } from 'src/app/RessourcesObject/SimplifiedPlaylist.object';
import { SimplifiedPlaylistService } from 'src/app/services/simplified-playlist.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  playlistList: SimplifiedPlaylistObject[] = [];

  constructor(private simplifiedPlaylistService: SimplifiedPlaylistService) { }

  ngOnInit(): void {
    this.simplifiedPlaylistService.getPlaylist().then(e => this.playlistList = e.items).catch(e => this.playlistList = []);
  }

}
