import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-navbar-link',
  templateUrl: './navbar-link.component.html',
  styleUrls: ['./navbar-link.component.css']
})
export class NavbarLinkComponent implements OnInit {
  isActive = false
  @Input() name: string;
  @Input() link: string;
  @Input() srcIcon: string;
  @Input() altIcon: string;
  constructor(private router: Router, private route: ActivatedRoute) {
    this.router.events.subscribe(url => {
      if (url instanceof NavigationStart) {
        const attachUrl = this.link.indexOf('/') !== 0 ? `/${this.link}` : this.link;
        this.isActive = attachUrl === url.url;
      }
    })
   }

  ngOnInit(): void {

  }

  onClick(e) {
    console.log('click');
    e.preventDefault();
    this.router.navigateByUrl(this.link);
  }

}
