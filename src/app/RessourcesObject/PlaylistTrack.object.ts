import { IEpisode } from "./Episode.object";
import { IPublicUser } from "./PublicUser.object";
import { ITrack } from "./Track.object";

export interface IPlaylistTrack {
  added_at: string | null; // WARN: Should be Timestamp;
  added_by: IPublicUser | null;
  is_local: boolean;
  track: ITrack | IEpisode ;
}
