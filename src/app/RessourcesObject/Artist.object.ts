import { IExternalUrl } from "./ExternalUrl.object";
import { IFollowersObject } from "./Followers.object";
import { IImage } from "./Image.object";
import { withID, withType, withURI } from "./tools.object";

export interface IArtist extends withID, withType, withURI {
  external_urls: IExternalUrl;
  followers: IFollowersObject;
  genres: string[];
  href: string;
  images: IImage[];
  name:string;
  popularity: number;
}
