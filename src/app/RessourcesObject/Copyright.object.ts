export interface ICopyright {
  text: string;
  type: string;
}
