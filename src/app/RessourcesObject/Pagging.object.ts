export interface IPaggingObject<T> {
  href: string,
  items: T[],
  limit: number,
  next: string | null,
  offset: number,
  previous: string | null,
  total: number,
}

export class PaggingObject<T> implements IPaggingObject<T> {
  href: string;
  items: T[];
  limit: number;
  next: string | null;
  offset: number;
  previous: string | null;
  total: number;
  constructor({href, items, limit, next, offset, previous, total}) {
    this.href = href;
    this.items = items;
    this.limit = limit;
    this.next = next;
    this.offset = offset;
    this.previous = previous;
    this.total = total;
  }

  static objectShouldHave(): string[] {
    return ['href', 'items', 'limit', 'next', 'offset', 'previous', 'total']
  }
}
