import { IFollowers } from "./Followers.object";
import { IPlaylistTrack } from "./PlaylistTrack.object";
import { IPublicUser } from "./PublicUser.object";
import { ISimplifiedPlaylist } from "./SimplifiedPlaylist.object";

export interface IPlaylist extends ISimplifiedPlaylist {
  followers: IFollowers;
  owner: IPublicUser;
}
