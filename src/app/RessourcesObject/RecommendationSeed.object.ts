import { withID, withType } from "./tools.object";

export interface IRecommendationSeed extends withID, withType {
  afterFilteringSize: number;
  afterRelinkingSize: number;
  href: string;
  initialPoolSize: number;
}
