import { IExplicitContentSettings } from "./ExplicitContentSettings.object";
import { IPublicUser } from "./PublicUser.object";

export interface IPrivateUser extends IPublicUser {
  country: string;
  email: string;
  explicit_content: IExplicitContentSettings;
  product: string;
}
