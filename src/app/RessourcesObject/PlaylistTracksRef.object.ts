export interface IPlaylistTracksRef {
  href: string;
  total: number;
}
