import { ICopyright } from "./Copyright.object";
import { IExternalUrl } from "./ExternalUrl.object";
import { IImage } from "./Image.object";
import { with_ID_URI_TYPE } from "./tools.object";

export interface ISimplifiedShow extends with_ID_URI_TYPE {
  available_markets: string[];
  copyrights: ICopyright[];
  description: string;
  explicit: boolean;
  external_urls: IExternalUrl;
  href: string;
  images: IImage[];
  is_externally_hosted: boolean;
  languages: string[];
  media_type: string;
  name: string;
  publisher: string;
}
