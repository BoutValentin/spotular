import { IImage } from "./Image.object";
import { IPlaylistTrack } from "./PlaylistTrack.object";
import { with_ID_URI_TYPE } from "./tools.object";

export interface ISimplifiedPlaylist extends with_ID_URI_TYPE {
  collaborative: boolean,
  description: string | null,
  external_urls: {
    [key: string]: string,
  },
  href: string,
  images: IImage[],
  name: string,
  public: null | boolean,
  snapshot_id: string,
  tracks: IPlaylistTrack[];
}

export class SimplifiedPlaylistObject implements ISimplifiedPlaylist {
  collaborative: boolean;
  description: string | null;
  external_urls: {
    [key: string]: string;
  };
  href: string;
  id: string;
  images: IImage[];
  name: string;
  public: null | boolean;
  snapshot_id: string;
  tracks: IPlaylistTrack[];

  type: string;
  uri: string;

  constructor(objec: ISimplifiedPlaylist) {
    Object.assign(this, objec);
  }

  static objectShouldHave(): string[] {
    return ['collaborative', 'description', 'external_urls', 'href', 'id', 'images', 'name', 'public', 'snapshot_id', 'tracks', 'type', 'uri']
  }
}
