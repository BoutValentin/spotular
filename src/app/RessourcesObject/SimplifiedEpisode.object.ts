import { IExternalUrl } from "./ExternalUrl.object";
import { IImage } from "./Image.object";
import { with_ID_URI_TYPE } from "./tools.object";
import { IEpisodeRestriction } from './EpisodeRestriction.object';
import { IResumePoint } from "./ResumePoint.object";

export interface ISimplifiedEpisode extends with_ID_URI_TYPE {
  audio_preview_url: string | null;
  description: string;
  duration_ms: number;
  explicit: boolean;
  external_urls: IExternalUrl;
  href: string;
  html_description: string;
  images: IImage[];
  is_externally_hosted: boolean;
  is_playable: boolean;
  language: string;
  languages: string[];
  name: string;
  release_date: string;
  release_date_precision: string;
  restrictions: IEpisodeRestriction
  resume_point:IResumePoint;
}
