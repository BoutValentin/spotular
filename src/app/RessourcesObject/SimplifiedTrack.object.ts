import { IExternalId } from "./ExternalId.object";
import { ILinkedTrack } from "./LinkedTrack.object";
import { ISimplifiedArtist } from "./SimplifiedArtist.object";
import { with_ID_URI_TYPE } from "./tools.object";
import { ITrackRestriction } from "./TrackRestriction.object";

export interface ISimplifiedTrack extends with_ID_URI_TYPE {
  artists: ISimplifiedArtist[];
  available_markets: string[];
  disc_number: number;
  duration_ms: number;
  explicit: boolean;
  external_urls: IExternalId;
  href: string;
  is_local: boolean;
  is_playable: boolean;
  linked_from: ILinkedTrack;
  name: string;
  preview_url: string;
  restrictions: ITrackRestriction
  track_number: number;
}
