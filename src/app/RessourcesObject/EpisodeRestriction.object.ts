export interface IEpisodeRestriction {
  reason: 'market' | 'product' | 'explicit' | string;
}
