import { IExternalUrl } from "./ExternalUrl.object";
import { IFollowers } from "./Followers.object";
import { IImage } from "./Image.object";
import { with_ID_URI_TYPE } from "./tools.object";

export interface IPublicUser extends with_ID_URI_TYPE {
  display_name: string;
  external_urls: IExternalUrl;
  followers: IFollowers;
  href: string;
  images: IImage[];
}

