import { IContext } from "./Context.object";
import { ISimplifiedTrack } from "./SimplifiedTrack.object";

export interface IPlayHistory {
  context: IContext;
  played_at: string; // WARN SHOULD BE TIMESTAMP
  track: ISimplifiedTrack;
}
