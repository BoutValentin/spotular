import { IContext } from "./Context.object";
import { IDevice } from "./Device.object";
import { IDisallows } from "./Disallows.object";
import { IEpisode } from "./Episode.object";
import { ITrack } from "./Track.object";

export interface ICurrentlyPlayingContext {
  actions: IDisallows;
  context: IContext | null;
  currently_playing_type: string;
  device: IDevice;
  is_playing: boolean;
  item: ITrack | IEpisode;
  progress_ms: number;
  repeat_state: string;
  shuffle_state: string;
  timestamp: number;
}
