import { IContext } from "./Context.object";
import { IEpisode } from "./Episode.object";
import { ITrack } from "./Track.object";

export interface ICurrentlyPlaying {
  context: IContext;
  currently_playing_type: string;
  is_playing: boolean;
  item: ITrack | IEpisode;
  progress_ms: number;
  timestamp: number;
}
