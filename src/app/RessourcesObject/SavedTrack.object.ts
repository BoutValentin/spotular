import { ITrack } from "./Track.object";

export interface ISavedShow {
  added_at: string; // WARN should be timestamp
  show: ITrack;
}
