import { IAlbumRestriction } from "./AlbumRestriction.object";
import { IExternalUrl } from "./ExternalUrl.object";
import { IImage } from "./Image.object";
import { ISimplifiedArtist } from "./SimplifiedArtist.object";
import { with_ID_URI_TYPE } from "./tools.object";

export interface ISimplifiedAlbum extends with_ID_URI_TYPE {
  album_group: string;
  album_type: string;
  artists: ISimplifiedArtist[];
  avaible_market: string[];
  external_urls: IExternalUrl;
  href: string;
  images: IImage[];
  name: string;
  release_date: string;
  releade_date_precision: string;
  restriction: IAlbumRestriction;
}
