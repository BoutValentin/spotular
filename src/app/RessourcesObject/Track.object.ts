import { ISimplifiedAlbum } from "./SimplifiedAlbum.object";

export interface ITrack {
  album: ISimplifiedAlbum;
  artists
}
