export interface IResumePoint {
  fully_played: boolean;
  resume_position_ms: number;
}
