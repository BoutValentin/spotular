import { IExternalUrl } from "./ExternalUrl.object";

export interface ISimplifiedArtist {
  external_url: IExternalUrl;
  href: string;
  id: string;
  name: string;
  type: string;
  uri: string;
}
