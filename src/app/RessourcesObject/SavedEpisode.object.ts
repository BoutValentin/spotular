import { IEpisode } from "./Episode.object";

export interface ISavedEpisode {
  added_at: string; // WARN Should be timestamp;
  track: IEpisode;
}
