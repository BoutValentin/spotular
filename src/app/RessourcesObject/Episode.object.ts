import { ISimplifiedEpisode } from "./SimplifiedEpisode.object";
import { ISimplifiedShow } from "./SimplifiedShow.object";

export interface IEpisode extends ISimplifiedEpisode {
  show: ISimplifiedShow;
}
