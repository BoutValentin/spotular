import { ICursor } from "./Cursor.object";

export interface ICursorPaging {
  cursors: ICursor;
  href: string;
  items: Object[];
  limit: number;
  next: string | null;
  total: number
}
