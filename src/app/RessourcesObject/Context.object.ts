import { IExternalUrl } from "./ExternalUrl.object";
import { withType, withURI } from "./tools.object";

export interface IContext extends withType, withURI{
  external_urls: IExternalUrl;
  href: string;
}
