export interface withID {
  id: string;
}

export interface withNullableID {
  id: string | null
}

export interface withURI {
  uri: string;
}

export interface withType {
  type: string;
}

export interface with_ID_URI_TYPE extends withID, withURI, withType {}
