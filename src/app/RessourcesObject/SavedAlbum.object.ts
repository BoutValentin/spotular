import { IAlbum } from "./Album.object";

export interface ISavedAlbum {
  added_at: string; // WARN Should be timestamp;
  album: IAlbum;
}
