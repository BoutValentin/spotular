export interface IExplicitContentSettings {
  filter_enabled: boolean;
  filter_locked: boolean;
}
