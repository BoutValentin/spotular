import { with_ID_URI_TYPE } from "./tools.object";

export interface IAudioFeatures extends with_ID_URI_TYPE {
  acousticness: number;
  analysis_url: string;
  danceability: number;
  duration_ms: number;
  energy: number;
  instrumentalness: number;
  key: number;
  liveness: number;
  loudness: number;
  mode: number;
  speechiness: number;
  tempo: number;
  time_signature: number;
  track_href: string;
  valence: number;
}
