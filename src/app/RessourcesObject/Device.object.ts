import { withNullableID, withType } from "./tools.object";

export interface IDevice extends withNullableID, withType{
  is_active: boolean;
  is_private_session: boolean;
  is_restricted: boolean;
  name: string;
  volume_percent: number;
}
