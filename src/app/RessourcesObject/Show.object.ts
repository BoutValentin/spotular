import { ISimplifiedEpisode } from "./SimplifiedEpisode.object";
import { ISimplifiedShow } from "./SimplifiedShow.object";

export interface IShow extends ISimplifiedShow {
  episodes: ISimplifiedEpisode[];
}
