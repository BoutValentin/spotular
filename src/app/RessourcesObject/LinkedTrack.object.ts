import { IExternalUrl } from "./ExternalUrl.object";
import { with_ID_URI_TYPE } from "./tools.object";

export interface ILinkedTrack extends with_ID_URI_TYPE {
  external_urls: IExternalUrl;
  href: string;
}
