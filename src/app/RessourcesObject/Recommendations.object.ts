import { IRecommendationSeed } from "./RecommendationSeed.object";
import { ISimplifiedTrack } from "./SimplifiedTrack.object";

export interface IRecommendations {
  seeds: IRecommendationSeed[];
  tracks: ISimplifiedTrack[];
}
