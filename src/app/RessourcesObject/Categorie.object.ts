import { IImage } from "./Image.object";
import { withID } from "./tools.object";

export interface ICategorie extends withID {
  href: string;
  icons: IImage[];
  name: string;
}
