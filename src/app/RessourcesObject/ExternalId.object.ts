export interface IExternalId {
  ean: string;
  isrc: string;
  upc: string;
}
