import { ISimplifiedShow } from "./SimplifiedShow.object";

export interface ISavedShow {
  added_at: string; // WARN should be timestamp
  show: ISimplifiedShow;
}
