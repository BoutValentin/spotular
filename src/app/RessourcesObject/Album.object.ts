import { IArtist } from "./Artist.object";
import { ICopyright } from "./Copyright.object";
import { IExternalId } from "./ExternalId.object";
import { ISimplifiedAlbum } from "./SimplifiedAlbum.object";
import { ISimplifiedTrack } from "./SimplifiedTrack.object";

export interface IAlbum extends ISimplifiedAlbum{
  copyrights: ICopyright[];
  external_ids: IExternalId;
  genres: string[];
  label: string;
  popularity: number;
  tracks: ISimplifiedTrack[];
}
