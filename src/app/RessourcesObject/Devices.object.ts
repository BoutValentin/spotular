import { IDevice } from "./Device.object";

export interface IDevices {
  devices: IDevice[]
}
