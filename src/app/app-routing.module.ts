import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumViewComponent } from './views/album-view/album-view.component';
import { ArtistViewComponent } from './views/artist-view/artist-view.component';
import { HomeViewComponent } from './views/home-view/home-view.component';
import { LibraryViewComponent } from './views/library-view/library-view.component';
import { LoginViewComponent } from './views/login-view/login-view.component';
import { NotFoundViewComponent } from './views/not-found-view/not-found-view.component';
import { PlaylistDetailViewComponent } from './views/playlist-detail-view/playlist-detail-view.component';
import { PlaylistViewComponent } from './views/playlist-view/playlist-view.component';
import { RedirectURILoginComponent } from './views/redirect-urilogin/redirect-urilogin.component';
import { SearchResultsViewComponent } from './views/search-results-view/search-results-view.component';
import { SearchViewComponent } from './views/search-view/search-view.component';

const routes: Routes = [
  {
    path: '',
    component: HomeViewComponent
  },
  {
    path: 'login',
    component: LoginViewComponent,
  },
  {
    path: 'redirect_uri',
    component: RedirectURILoginComponent,
  },
  {
    path: 'search',
    component: SearchViewComponent
  },
  {
    path: 'search/:query',
    component: SearchResultsViewComponent,
  },
  {
    path: 'album/:albumId',
    component: AlbumViewComponent,
  },
  {
    path: 'artist/:artistid',
    component: ArtistViewComponent,
  },
  {
    path: 'playlist',
    component: PlaylistViewComponent,
  },
  {
    path: 'playlist/:playlistId',
    component: PlaylistDetailViewComponent
  },
  {
    path: 'library',
    component: LibraryViewComponent,
  },
  {
    path: '**',
    component: NotFoundViewComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
