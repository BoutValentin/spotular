import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistDetailViewComponent } from './playlist-detail-view.component';

describe('PlaylistDetailViewComponent', () => {
  let component: PlaylistDetailViewComponent;
  let fixture: ComponentFixture<PlaylistDetailViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaylistDetailViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
