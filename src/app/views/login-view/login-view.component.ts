import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import {SHA256, enc} from 'crypto-js';

const SCOPES = [
  'playlist-read-private',
  'playlist-read-collaborative'
]
@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    //console.log('after app ?')
    const code_challenge = SHA256(window.localStorage.getItem("code_verifier")).toString(enc.Base64);
    window.location.href = `https://accounts.spotify.com/authorize?client_id=${environment.spotifyClientID}&response_type=code&redirect_uri=${environment.clientUrl}/redirect_uri&code_challenge_method=S256&code_challenge=${code_challenge.replace(/=/g, '')}&scope=${encodeURIComponent(SCOPES.join(' '))}`
  }

}
