import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectURILoginComponent } from './redirect-urilogin.component';

describe('RedirectURILoginComponent', () => {
  let component: RedirectURILoginComponent;
  let fixture: ComponentFixture<RedirectURILoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedirectURILoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectURILoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
