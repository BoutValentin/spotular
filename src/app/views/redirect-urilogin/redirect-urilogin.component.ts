import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-redirect-urilogin',
  templateUrl: './redirect-urilogin.component.html',
  styleUrls: ['./redirect-urilogin.component.css']
})
export class RedirectURILoginComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (params['error'] || !params['code']) {
        this.router.navigateByUrl("/?error=true");
        return;
      }
      const code = params['code'];
      const body_object = {
        'client_id': environment.spotifyClientID,
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': `${environment.clientUrl}/redirect_uri`,
        'code_verifier': window.localStorage.getItem("code_verifier")
      };
      const body_array = [];
      for (const [key, value] of Object.entries(body_object)) {
        body_array.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`);
      }
      console.log(body_array.join('&'))
      fetch('https://accounts.spotify.com/api/token', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: body_array.join('&')
      }).then((result) => result.json()).then((resp) => {
        window.localStorage.setItem('access_token', resp['access_token']);
        window.localStorage.setItem('request_at', Date.toString());
        window.localStorage.setItem('expires_in', resp['expires_in']);
        window.localStorage.setItem('refresh_token', resp['refresh_token']);
        console.log(resp);
        this.router.navigateByUrl('/');
      }
      ).catch(() => {
        this.router.navigateByUrl("/?error=true");
      });
      })
  }

}
