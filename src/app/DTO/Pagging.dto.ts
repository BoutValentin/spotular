import { DTO, receiveObject } from './Dto.dto';
import { IPaggingObject, PaggingObject } from '../RessourcesObject/Pagging.object';

export class PaggingDTO<T, P extends DTO<T>> implements DTO<IPaggingObject<T>, P> {

  fromJSON(resp: receiveObject, membersDTO: P): IPaggingObject<T> {
    if (!resp.items || !Array.isArray(resp.items)) throw new Error("menbers should be an array of value");
    const membersParse = [];
    for (const items of resp.items) {
      membersParse.push(membersDTO.fromJSON(items))
    }
    if (!this.isPagging(resp)) throw new Error("resp is missing propreties")
    return new PaggingObject({...resp, items: membersParse});
  }

  isPagging(resp: any): resp is PaggingObject<T> {
    return PaggingObject.objectShouldHave().reduce((prev, prop) => prev && ( prop in resp), true);
  }
}
