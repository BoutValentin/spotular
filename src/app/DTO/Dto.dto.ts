export type receiveObject = {
  [key: string]: receiveObject | unknown
}
export interface DTO<T, P = {}> {
  fromJSON: (resp: receiveObject, membersDTO?: P) => T,
}
