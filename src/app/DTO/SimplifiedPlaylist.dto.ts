import { SimplifiedPlaylistObject } from "../RessourcesObject/SimplifiedPlaylist.object";
import { DTO, receiveObject } from "./Dto.dto";

export class SimplifiedPlaylistDTO implements DTO<SimplifiedPlaylistObject>{

  fromJSON(resp: receiveObject): SimplifiedPlaylistObject {
    if (!this.isPagging(resp)) throw new Error("resp is missing propreties")
    return new SimplifiedPlaylistObject({...resp});
  }

  isPagging(resp: any): resp is SimplifiedPlaylistObject {
    return SimplifiedPlaylistObject.objectShouldHave().reduce((prev, prop) => prev && ( prop in resp), true);
  }
}
