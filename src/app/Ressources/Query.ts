export class Query {
  queries: Map<string, string|string[]>

  /**
   * Create a Query object
   * @param defaultQueryInit a callback function to init query
   */
  constructor(defaultQueryInit: () => void = () => {}) {
    this.queries = new Map<string, string|string[]>();
    this.defaultQueryInit = defaultQueryInit;
    this.defaultQueryInit();
  }

  /**
   * Add the parameter to the query, if the query already exists, transform to an array
   * If replace is set to true: only replace the value
   *
   * @param key string
   * @param value string
   * @param replace boolean
   */
  addParamToQuery(key: string, value: string, replace: boolean = false) {
    const currentValue = this.queries.get(key);
    if(replace || !currentValue) {
      this.queries.set(key, encodeURIComponent(value));
      return;
    }
    if (Array.isArray(currentValue)) {
      if (currentValue.indexOf(value) >= 0) return;
      this.queries.set(key, [...currentValue, encodeURIComponent(value)])
      return;
    }
    if (typeof currentValue === 'string') {
      this.queries.set(key, [currentValue, encodeURIComponent(value)])
      return;
    }
  }

  /**
   *  Replace the key parameter to the query
   *
   * @param key string the key of the query to replace
   * @param value string the value to set for the query
   */
  addAndReplaceToQuery(key: string, value: string) {
    this.addParamToQuery(key, value, true);
  }

  /**
   *  Remove one key to the query, if value is set, remove only that value if it's contain or equals.
   *
   * @param key string the key to remove
   * @param value string if set remove only that value from string or if current value is equals
   * @returns
   */
  removeParamToQuery(key: string, value: string = ''): string | string[] | null | undefined {
    const currentValue = this.queries.get(key);
    if (!currentValue) return currentValue;
    if (!value || currentValue === value) {
      this.queries.delete(key);
      return currentValue;
    }
    if (Array.isArray(currentValue)) {
      const idx = currentValue.indexOf(value);
      if (idx < 0) return currentValue;
      currentValue.splice(idx, 1);
      this.queries.set(key, currentValue);
      return currentValue;
    }
  }
  /**
   * Surcharging methods to String
   * Exemple: ?foo=bar,foo&bar=foo
   * @returns string
   */
  toString(): string {
    const res = this.getKeyValueInArrayReadyToString();
    return res.length <= 0 ? '' : `?${res.join('&')}`;
  }

  /**
   * ToString methods but use duplicate key possibilities
   * Exemple: ?foo=bar&foo=foo
   * @returns string
   */
  toStringDuplicateKey(): string {
    const res = this.getKeyValueInArrayReadyToStringWithDuplicateKey();
    return res.length <= 0 ? '' : `?${res.join('&')}`;
  }
  /**
   * Return an array with all key and value join
   * @returns string[] an array with each value equals to 'key=value(,value)'
   */
  getKeyValueInArrayReadyToString() {
    const callback = (key: string, value: string|string[]) => Array.isArray(value) ? `${key}=${value.join(',')}` : `${key}=${value}`
    return this.abstractMappingKeyValue(callback);
  }

  /**
   * Return an array with potential duplicated key
   * @returns string[] an array with each value equals to 'key=value'
   */
  getKeyValueInArrayReadyToStringWithDuplicateKey() {
    const callback = (key: string, value: string|string[]) => Array.isArray(value) ? value.map((valueArr) => `${key}=${valueArr}`).join('&') : `${key}=${value}`;
    return this.abstractMappingKeyValue(callback);
  }

  private abstractMappingKeyValue(callback: (key: string, value: string|string[]) => string, iterable = this.queries.entries()) {
    const res = [];
    for (const [key, value] of iterable){
      res.push(callback(key, value));
    }
    return res;
  }

  /**
   * Clear all the current query
   */
  resetQuery(): void {
    this.queries.clear();
  }

  /**
   * Reset and recall the init callback provide in constructor
   */
  resetQueryAndInit(): void {
    this.queries.clear();
    this.defaultQueryInit();
  }

  /**
   * Default function call, do nothing by default
   */
  defaultQueryInit():void { }

}
