import { environment } from "src/environments/environment";
import { DTO } from "../DTO/Dto.dto";
import { PaggingDTO } from "../DTO/Pagging.dto";
import { IPaggingObject } from "../RessourcesObject/Pagging.object";
import { Query } from "./Query";

const API_URL = environment.spotifyApiUrl

export class Ressources<T, P = {}> {
  API_URL = API_URL;
  entrypoint: string;
  dto: DTO<T, P>;
  query: Query;
  disabledQuery: boolean;

  constructor(entrypoint, dto) {
    this.entrypoint = entrypoint;
    this.dto = dto;
    this.query = new Query();
    this.disabledQuery = false;
  }

  async getAll(): Promise<IPaggingObject<T>> {
    try {
      const res = await fetch(`${this.API_URL}${this.entrypoint}${this.getQueryString()}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${window.localStorage.getItem('access_token')}`
        }
      });
      const ressources = await res.json();
      return new PaggingDTO<T, DTO<T>>().fromJSON(ressources, this.dto);
    } catch(e) {
      return Promise.reject(e);
    }
  }

  async getOne(id: string): Promise<T> {
    try {
      const res = await fetch(`${this.API_URL}${this.entrypoint}/${id}${this.getQueryString()}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${window.localStorage.getItem('access_token')}`
        }
      });
      const ressources = await res.json();
      return this.dto.fromJSON(ressources);
    } catch(e) {
      return Promise.reject(e)
    }
  }

  getQueryString(): string {
    return this.disabledQuery ? '' : this.query.toString();
  }
}
