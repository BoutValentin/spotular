import { SimplifiedPlaylistDTO } from "../DTO/SimplifiedPlaylist.dto";
import { SimplifiedPlaylistObject } from "../RessourcesObject/SimplifiedPlaylist.object";
import { Ressources } from "./Ressources";

export class SimplifiedPlaylistRessources extends Ressources<SimplifiedPlaylistObject> {
  constructor() {
    super('/me/playlists', new SimplifiedPlaylistDTO());
  }
}
