import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeViewComponent } from './views/home-view/home-view.component';
import { SearchViewComponent } from './views/search-view/search-view.component';
import { SearchResultsViewComponent } from './views/search-results-view/search-results-view.component';
import { LibraryViewComponent } from './views/library-view/library-view.component';
import { ArtistViewComponent } from './views/artist-view/artist-view.component';
import { AlbumViewComponent } from './views/album-view/album-view.component';
import { NotFoundViewComponent } from './views/not-found-view/not-found-view.component';
import { LoginViewComponent } from './views/login-view/login-view.component';
import { RedirectURILoginComponent } from './views/redirect-urilogin/redirect-urilogin.component';
import { NavbarLinkComponent } from './components/navbar-link/navbar-link.component';
import { PlaylistViewComponent } from './views/playlist-view/playlist-view.component';
import { PlaylistDetailViewComponent } from './views/playlist-detail-view/playlist-detail-view.component';
import { NavbarPlaylistLinkComponent } from './components/navbar-playlist-link/navbar-playlist-link.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeViewComponent,
    SearchViewComponent,
    SearchResultsViewComponent,
    LibraryViewComponent,
    ArtistViewComponent,
    AlbumViewComponent,
    NotFoundViewComponent,
    LoginViewComponent,
    RedirectURILoginComponent,
    NavbarLinkComponent,
    PlaylistViewComponent,
    PlaylistDetailViewComponent,
    NavbarPlaylistLinkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
