import { Component } from '@angular/core';

const MIN_ASCII = 32;
const MAX_ASCII = 126 - MIN_ASCII;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'spotular';

  ngOnInit() {
    if ( !window.localStorage.getItem('code_verifier')) this.createCodeVerifier(/[A-Za-z0-9_\-~]/)
  }

  private generateStrRandom(regex: RegExp, min = 43, max = 128 - 43) : string {
    let str = '';
    const unacpetedValue = new Map<string, boolean>();
    const numberOfChar = Math.floor(Math.random() * max) + min;
    while (str.length < numberOfChar) {
      const char = String.fromCharCode(Math.floor(Math.random()  * MAX_ASCII) + MIN_ASCII);
      !unacpetedValue.get(char) && regex.test(char) ? str += char : unacpetedValue.set(char, true);
    }
    return str;
  }

  private createCodeVerifier(regex: RegExp, min = 43, max = 128 - 43) {
    window.localStorage.setItem('code_verifier', this.generateStrRandom(regex, min, max));
  }
}
