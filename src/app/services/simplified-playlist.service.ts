import { Injectable } from '@angular/core';
import { SimplifiedPlaylistRessources } from '../Ressources/SimplifiedPlaylistRessources';
import { IPaggingObject } from '../RessourcesObject/Pagging.object';
import { SimplifiedPlaylistObject } from '../RessourcesObject/SimplifiedPlaylist.object';

@Injectable({
  providedIn: 'root'
})
export class SimplifiedPlaylistService {
  playlist: null|IPaggingObject<SimplifiedPlaylistObject> = null;
  lastFetch: null|number = null;
  constructor() {
  }

  getPlaylist(): Promise<null |IPaggingObject<SimplifiedPlaylistObject>> {
    if (this.playlist && this.lastFetch && this.lastFetch < Date.now() + 3600) return Promise.resolve(this.playlist);
    return new Promise((resolve, reject) => new SimplifiedPlaylistRessources().getAll().then(res=>{
      this.playlist = res;
      console.log(res);
      this.lastFetch = Date.now();
      return resolve(this.playlist);
    }).catch(e=>{reject(e);return this.playlist}))
  }
}
