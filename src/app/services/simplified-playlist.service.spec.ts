import { TestBed } from '@angular/core/testing';

import { SimplifiedPlaylistService } from './simplified-playlist.service';

describe('PlaylistService', () => {
  let service: SimplifiedPlaylistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SimplifiedPlaylistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
